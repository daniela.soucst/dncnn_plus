import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np
import PIL
import cv2, os
from torchvision import transforms

average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

img_transform = transforms.Compose([transforms.ToTensor(),
                                    transforms.Normalize(average_color, std_color),
                                    ])

######################################################################
class DenoisingDataset_Color(Dataset):
    """Dataset wrapping tensors.
    Arguments:
        xs (Tensor): clean image patches
        sigma: noise level, e.g., 25
    """
    def __init__(self, xs, ground_truth):
        super(DenoisingDataset_Color, self).__init__()

        self.xs = xs
        # self.file_list = file_list
        self.ground_truth = ground_truth

    def __getitem__(self, index):

        # img_dir = self.file_list[index]
        batch_x = self.xs[index]
        est = self.ground_truth[index]

        # batch_x = batch_x.ToPILImage()
        batch_x, batch_y = image_correction(np.array(batch_x), np.asarray(est))

        # batch_x = batch_x.ToTensor()
        # batch_y = batch_y.ToTensor()

        # batch_x = PIL.Image.fromarray(batch_x.astype(np.uint8))
        # batch_y = PIL.Image.fromarray(batch_y.astype(np.uint8))
        #
        batch_x = img_transform(batch_x) #transforma de ndarray para tensor
        batch_y = img_transform(batch_y)#transforma de ndarray para tensor

        return batch_x,batch_y

    def __len__(self):
        return self.xs.size(0)
######################################################################

def image_correction(im, est):
    # im2 = cv2.imread('data/original/8D5U5524.tif')
    # im2 = cv2.cvtColor(im2, cv2.COLOR_BGR2RGB)
    im = im.transpose(2, 1, 0)  # (channel,h,w)
    img = (im / 255.0) ** 2.2 * 65536

    corrected = np.power(img / 65535 / est[None, None, :] * np.mean(est), 1 / 2.2)[:, :, ::-1]

    # cv2.imwrite(os.path.join(save_dir, img_name+'.png'), corrected * 255)
    # io.imsave(os.path.join(save_dir, img_name+'.png'), corrected * 255)
    im = im.astype(np.uint8)
    corrected = corrected.astype(np.uint8)
    return im,corrected*255