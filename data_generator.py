# -*- coding: utf-8 -*-

# =============================================================================
#  @article{zhang2017beyond,
#    title={Beyond a {Gaussian} denoiser: Residual learning of deep {CNN} for image denoising},
#    author={Zhang, Kai and Zuo, Wangmeng and Chen, Yunjin and Meng, Deyu and Zhang, Lei},
#    journal={IEEE Transactions on Image Processing},
#    year={2017},
#    volume={26},
#    number={7},
#    pages={3142-3155},
#  }
# by Kai Zhang (08/2018)
# cskaizhang@gmail.com
# https://github.com/cszn
# modified on the code from https://github.com/SaoYan/DnCNN-PyTorch
# =============================================================================

# no need to run this code separately


import glob
import cv2
import numpy as np
# from multiprocessing import Pool
from torch.utils.data import Dataset
import torch
import PIL
import os
from torchvision import transforms

patch_size, stride = 40, 10
aug_times = 1
scales = [1, 0.9, 0.8, 0.7]
batch_size = 128
average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

img_transform = transforms.Compose([transforms.ToTensor(),
                                    transforms.Normalize(average_color, std_color),
                                    ])
# class DenoisingDataset(Dataset):
#     """Dataset wrapping tensors.
#     Arguments:
#         xs (Tensor): clean image patches
#         sigma: noise level, e.g., 25
#     """
#     def __init__(self, root_dir, file_list, ground_truth, sigma, transform=None):
#         super(DenoisingDataset, self).__init__()
#
#         self.root_dir = root_dir
#         self.file_list = file_list
#         self.sigma = sigma
#         self.transform = transform
#         self.ground_truth = ground_truth
#
#     def __getitem__(self, index):
#
#         img_dir = self.file_list[index]
#         est = self.ground_truth[index]
#
#         batch_x = cv2.cvtColor(cv2.imread(os.path.join(self.root_dir,img_dir+'.tif')), cv2.COLOR_BGR2RGB)  # RGB scale
#         batch_x = batch_x.astype(np.uint8)
#         batch_x = PIL.Image.fromarray(batch_x)
#
#         if self.transform:
#             batch_x = self.transform(batch_x)
#
#
#         batch_x, batch_y = image_correction(np.array(batch_x), np.asarray(est))
#
#         batch_x = PIL.Image.fromarray(batch_x.astype(np.uint8))
#         batch_y = PIL.Image.fromarray(batch_y.astype(np.uint8))
#         #
#         batch_x = img_transform(batch_x) #transforma de ndarray para tensor
#         batch_y = img_transform(batch_y)#transforma de ndarray para tensor
#
#         return {'original_img':batch_x, 'corrected_img':batch_y}
#
#     def __len__(self):
#         return len(self.file_list)

# def image_correction(im, est):
#     # im = cv2.imread(image_dir)
#     # im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
#
#     img = (im / 255.0) ** 2.2 * 65536
#
#     corrected = np.power(img / 65535 / est[None, None, :] * np.mean(est), 1 / 2.2)[:, :, ::-1]
#
#     # cv2.imwrite(os.path.join(save_dir, img_name+'.png'), corrected * 255)
#     # io.imsave(os.path.join(save_dir, img_name+'.png'), corrected * 255)
#     return im,corrected*255


def show(x, title=None, cbar=False, figsize=None):
    import matplotlib.pyplot as plt
    plt.figure(figsize=figsize)
    plt.imshow(x, interpolation='nearest', cmap='gray')
    if title:
        plt.title(title)
    if cbar:
        plt.colorbar()
    plt.show()


def data_aug(img, mode=0):
    # data augmentation
    if mode == 0:
        return img
    elif mode == 1:
        return np.flipud(img)
    elif mode == 2:
        return np.rot90(img)
    elif mode == 3:
        return np.flipud(np.rot90(img))
    elif mode == 4:
        return np.rot90(img, k=2)
    elif mode == 5:
        return np.flipud(np.rot90(img, k=2))
    elif mode == 6:
        return np.rot90(img, k=3)
    elif mode == 7:
        return np.flipud(np.rot90(img, k=3))


def gen_patches(file_name, colored=False):
    # get multiscale patches from a single image
    patches = []
    if(colored==False):
        img = cv2.imread(file_name, 0)  # gray scale

        h, w = img.shape
    else:
        img = cv2.cvtColor(cv2.imread(file_name), cv2.COLOR_BGR2RGB)  # RGB scale
        img = cv2.resize(img, (180, 180), interpolation=cv2.INTER_CUBIC)
        h, w, _ = img.shape
        # img_scaled = cv2.resize(img, (180, 180), interpolation=cv2.INTER_CUBIC)
    for s in scales:
        h_scaled, w_scaled = int(h*s), int(w*s)
        img_scaled = cv2.resize(img, (h_scaled, w_scaled), interpolation=cv2.INTER_CUBIC)

        # extract patches
        for i in range(0, h_scaled-patch_size+1, stride):
            for j in range(0, w_scaled-patch_size+1, stride):
                x = img_scaled[i:i+patch_size, j:j+patch_size]
                if(x.shape[0] == patch_size and x.shape[1]== patch_size):
                    for k in range(0, aug_times):
                        x_aug = data_aug(x, mode=np.random.randint(0, 8))
                        patches.append(x_aug)

    return patches


def datagenerator(data_dir='data/Train400', img_names=None, ground_truth=None, fmt='.tif', verbose=False, colored_patch=False):
    # generate clean patches from a dataset

    print('datagenerator: ',data_dir,len(img_names))
    # initrialize
    data = []
    gd_list = []
    # generate patches
    for i in range(len(img_names)):
        patches = gen_patches(os.path.join(data_dir,img_names[i]+fmt), colored_patch)
        for patch in patches:
            # print('patch', patch.shape)
            data.append(patch)
            gd_list.append(ground_truth[i])
        if verbose:
            print(str(i+1) + '/' + str(len(img_names)) + ' is done ^_^')
    data = np.array(data, dtype='uint8')
    # data = np.asarray(data)
    if(colored_patch==False):
      data = np.expand_dims(data, axis=3)
      discard_n = len(data)-len(data)//batch_size*batch_size  # because of batch namalization
      data = np.delete(data, range(discard_n), axis=0)
    print('^_^-training data finished-^_^')
    return data,np.asarray(gd_list)


if __name__ == '__main__':

    data = datagenerator(data_dir='data/Train400')


#    print('Shape of result = ' + str(res.shape))
#    print('Saving data...')
#    if not os.path.exists(save_dir):
#            os.mkdir(save_dir)
#    np.save(save_dir+'clean_patches.npy', res)
#    print('Done.')
