# -*- coding: utf-8 -*-

# PyTorch 0.4.1, https://pytorch.org/docs/stable/index.html

# =============================================================================
#  @article{zhang2017beyond,
#    title={Beyond a {Gaussian} denoiser: Residual learning of deep {CNN} for image denoising},
#    author={Zhang, Kai and Zuo, Wangmeng and Chen, Yunjin and Meng, Deyu and Zhang, Lei},
#    journal={IEEE Transactions on Image Processing},
#    year={2017},
#    volume={26}, 
#    number={7}, 
#    pages={3142-3155}, 
#  }
# by Kai Zhang (08/2018)
# cskaizhang@gmail.com
# https://github.com/cszn
# modified on the code from https://github.com/SaoYan/DnCNN-PyTorch
# =============================================================================

# run this to train the model

# =============================================================================
# For batch normalization layer, momentum should be a value from [0.1, 1] rather than the default 0.1. 
# The Gaussian noise output helps to stablize the batch normalization, thus a large momentum (e.g., 0.95) is preferred.
# =============================================================================

import argparse
import re
import os, glob, datetime, time
import numpy as np
import torch
import torch.nn as nn
from torch.nn.modules.loss import _Loss
import torch.nn.init as init
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR
import scipy
import data_generator as dg
from dataset_colored import DenoisingDataset_Color
import util
from dataset_gray import DenoisingDataset_Gray
# Params
parser = argparse.ArgumentParser(description='PyTorch DnCNN')
parser.add_argument('--model', default='DnCNN', type=str, help='choose a type of model')
parser.add_argument('--batch_size', default=50, type=int, help='batch size')
# parser.add_argument('--train_data', default='data/train_colored', type=str, help='path of train data')
parser.add_argument('--train_data', default='data/original', type=str, help='path of train data')
# parser.add_argument('--train_data', default='data/train', type=str, help='path of train data')

parser.add_argument('--sigma', default=25, type=int, help='noise level')
# parser.add_argument('--epoch', default=180, type=int, help='number of train epoches')
parser.add_argument('--epoch', default=2, type=int, help='number of train epoches')
parser.add_argument('--lr', default=1e-3, type=float, help='initial learning rate for Adam')
parser.add_argument('--color', default=True, type=bool, help='color images or not')
# parser.add_argument('--color', default=False, type=bool, help='color images or not')
parser.add_argument('--indexes_dir', default='data/train_indexes',
                    type=str, help='the model name')
parser.add_argument('--folder', default=0, type=int, help='folder index')


args = parser.parse_args()

batch_size = args.batch_size
cuda = torch.cuda.is_available()
n_epoch = args.epoch
sigma = args.sigma
device = 'cuda' if torch.cuda.is_available() else 'cpu'

average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

save_dir = os.path.join('models', args.model+'_' + 'sigma' + str(sigma))

if not os.path.exists(save_dir):
    os.mkdir(save_dir)


class DnCNN(nn.Module):
    def __init__(self, depth=17, n_channels=64, image_channels=1, use_bnorm=True, kernel_size=3):
        super(DnCNN, self).__init__()
        kernel_size = 3
        padding = 1
        layers = []

        layers.append(nn.Conv2d(in_channels=image_channels, out_channels=n_channels, kernel_size=kernel_size, padding=padding, bias=True))
        layers.append(nn.ReLU(inplace=True))
        for _ in range(depth-2):
            layers.append(nn.Conv2d(in_channels=n_channels, out_channels=n_channels, kernel_size=kernel_size, padding=padding, bias=False))
            layers.append(nn.BatchNorm2d(n_channels, eps=0.0001, momentum = 0.95))
            layers.append(nn.ReLU(inplace=True))
        layers.append(nn.Conv2d(in_channels=n_channels, out_channels=image_channels, kernel_size=kernel_size, padding=padding, bias=False))
        self.dncnn = nn.Sequential(*layers)
        self._initialize_weights()

    def forward(self, x):
        y = x
        out = self.dncnn(x)
        return y-out

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                init.orthogonal_(m.weight)
                print('init weight')
                if m.bias is not None:
                    init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                init.constant_(m.weight, 1)
                init.constant_(m.bias, 0)


class sum_squared_error(_Loss):  # PyTorch 0.4.1
    """
    Definition: sum_squared_error = 1/2 * nn.MSELoss(reduction = 'sum')
    The backward is defined as: input-target
    """
    def __init__(self, size_average=None, reduce=None, reduction='sum'):
        super(sum_squared_error, self).__init__(size_average, reduce, reduction)

    def forward(self, input, target):
        # return torch.sum(torch.pow(input-target,2), (0,1,2,3)).div_(2)
        return torch.nn.functional.mse_loss(input, target, size_average=None, reduce=None, reduction='sum').div_(2)


def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir, 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch


def log(*args, **kwargs):
     print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:"), *args, **kwargs)


if __name__ == '__main__':
    # model selection
    print('===> Building model')
    if(args.color):
        model = DnCNN(image_channels=3)
    else:
        model = DnCNN(image_channels=1)
    
    initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style
    if initial_epoch > 0:
        print('resuming by loading epoch %03d' % initial_epoch)
        # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
        model = torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch))

    from torchvision import transforms

    gehler = {'root_dir': 'data/original',
              'ground_truth': 'data/srgb8bit_whitepoints.txt'
              }
    img_name_list, ground_truth = util.get_names_and_gd(gehler['ground_truth'])

    train_names = scipy.io.loadmat(os.path.join(args.indexes_dir, 'train_set' + str(args.folder) + '.mat'))['train_names']

    model.train()
    # criterion = nn.MSELoss(reduction = 'sum')  # PyTorch 0.4.1
    criterion = sum_squared_error()
    if cuda:
        model = model.cuda()
         # device_ids = [0]
         # model = nn.DataParallel(model, device_ids=device_ids).cuda()
         # criterion = criterion.cuda()
    optimizer = optim.Adam(model.parameters(), lr=args.lr)
    scheduler = MultiStepLR(optimizer, milestones=[30, 60, 90], gamma=0.2)  # learning rates

    for epoch in range(initial_epoch, n_epoch):

        scheduler.step(epoch)  # step to the learning rate in this epcoh
        xs_all,gd_all = dg.datagenerator(data_dir=args.train_data, img_names = train_names, ground_truth=ground_truth, colored_patch=args.color)
        # xs = dg.datagenerator(data_dir=args.train_data, colored_patch=True)
        salto = int(xs_all.shape[0]/3)
        mini_epoch = 1
        for k in range(0,xs_all.shape[0],salto):
            xs = xs_all[k:k+salto]
            xs = xs.astype('float32') / 255.0
            xs = torch.from_numpy(xs.transpose((0, 3, 1, 2)))  # tensor of the clean patches, NXCXHXW

            gd = gd_all[k:k+salto]
            if(args.color):
                DDataset = DenoisingDataset_Color(xs, ground_truth=gd)
            else:
                DDataset = DenoisingDataset_Gray(xs, sigma)

            DLoader = DataLoader(dataset=DDataset, num_workers=4, drop_last=True, batch_size=batch_size, shuffle=True)
            epoch_loss = 0
            start_time = time.time()

            for n_count, batch_yx in enumerate(DLoader):
                optimizer.zero_grad()
                if cuda:
                    batch_x, batch_y = batch_yx[0].cuda(), batch_yx[1].cuda()
                loss = criterion(model(batch_y), batch_x)
                epoch_loss += loss.item()
                loss.backward()
                optimizer.step()

                if n_count % 10 == 0:
                    print('epoch: %4d mini_epoch:  %4d/%4d   %4d / %4d loss = %2.4f' % (
                    epoch + 1, mini_epoch,3, n_count, xs.size(0) // batch_size, loss.item() / batch_size))
            mini_epoch += 1
            elapsed_time = time.time() - start_time

            # log('epcoh = %4d , loss = %4.4f , time = %4.2f s' % (epoch + 1, epoch_loss / n_count, elapsed_time))
            # np.savetxt('train_result.txt', np.hstack((epoch + 1, epoch_loss / n_count, elapsed_time)), fmt='%2.4f')
            # # torch.save(model.state_dict(), os.path.join(save_dir, 'model_%03d.pth' % (epoch+1)))
            torch.save(model, os.path.join(save_dir, 'model_%03d.pth' % (epoch + 1)))

            print('batch_idx: %4d | epoca: %4d | Loss: %.3f ' % (
                        n_count, epoch + 1, epoch_loss / (n_count + 1)))

